Check out thrid party libs (just fmt):

```sh
git submodule update --init --recursive
```

Compile with:

```sh
mkdir build && \
    cd build && \
    cmake .. && \
    make
```

Run with (inside `build` dir):

```sh
srun -N 2 --tasks-per-node=4 --gres=gpu:2 --time=00:01:00 -u gpu_assignment
```

With MPS (inside same directory as `mps-wrapper.sh`):

```sh
srun --mem-per-cpu=1G --tasks-per-node=4 --gres=gpu:2 --nodes=2 -u -l ./mps-wrapper.sh ./build/gpu_assignment
```
