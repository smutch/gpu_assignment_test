#include <fmt/format.h>
#include <helper_cuda.h>
#include <mpi.h>


int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int world_rank = -1;
    int world_size = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // find the local rank on this node
    MPI_Info info;
    MPI_Info_create(&info);

    MPI_Comm local_comm;
    MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, world_rank, info, &local_comm);

    int local_rank = -1;
    MPI_Comm_rank(local_comm, &local_rank);

    int local_size = -1;
    MPI_Comm_size(local_comm, &local_size);

    MPI_Comm_free(&local_comm);
    MPI_Info_free(&info);

    // get the number of devices available to this rank
    int num_devices = 0;
    checkCudaErrors( cudaGetDeviceCount(&num_devices) );

    // do your thang to assign this rank to a device
    // checkCudaErrors( cudaSetDevice(local_rank % num_devices) );  // round robin
    checkCudaErrors( cudaSetDevice((local_rank * num_devices / local_size) % num_devices) );  // consecutive ranks share
    
    // print our assignments
    for(int ii=0; ii < world_size; ++ii) {
        if (world_rank == ii) {
            int my_device;
            checkCudaErrors( cudaGetDevice(&my_device) );
            fmt::print("Rank {} ({}/{}) assigned to GPU device {} of {}\n",
                    world_rank, local_rank, local_size, my_device, num_devices);
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    // do a malloc on the device to check all is well with the world
    float *d_temp;
    if (cudaMalloc((void**)&d_temp, sizeof(float)) != cudaSuccess)
        fmt::print(stderr, "cudaMalloc error on rank {}\n", world_rank);
    else
        checkCudaErrors( cudaFree(d_temp) );

    MPI_Finalize();

    return EXIT_SUCCESS;
}
